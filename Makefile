DRAFT_BASENAME = ietf-chempat

OUTPUTS = $(DRAFT_BASENAME).txt $(DRAFT_BASENAME).html $(DRAFT_BASENAME).xml $(DRAFT_BASENAME).pdf

all: $(OUTPUTS)

%.xml: %.md
	kramdown-rfc2629 --v3 $< > $@.tmp
	mv $@.tmp $@

%.html: %.xml
	xml2rfc --v3 $< --html

%.txt: %.xml
	xml2rfc --v3 $< --text

%.pdf: %.xml
	xml2rfc --v3 $< --pdf

clean:
	-rm -rfv .*~ *~ *.tmp *.xml *.html *.pdf *.txt public .refcache

public:
	mkdir -pv public
	cp -v ietf-* public/
	rm -fv public/index.html
	for f in ietf-*; do echo "<li><a href=\"$$f\">$$f</a>" >> public/index.html; done

.PHONY: all clean public
