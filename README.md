# Chempat

The latest output can be read here:
https://jas.gitlab.io/ietf-chempat

Submitted versions can be found here:
https://datatracker.ietf.org/doc/draft-josefsson-chempat/
